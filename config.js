var config = {};

config.port = process.env.WEB_PORT || 8080;

config.host = process.env.SERVER_NAME || 'localhost'

config.music_root = 'music';

config.version = '1.5.0';

module.exports = config;


