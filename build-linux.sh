#!/bin/bash

npm install pkg 
npm install encoding iconv-lite node-fetch node-fs-extra utf8 valid-url xmldom node-fetch node-fs-extra replace-in-file extract-zip

declare -a archs=("win" "mac" "linux")

for i in "${archs[@]}"
do
   echo "Building for $i now."
   if [ "$i" == "win" ]
   then
    node_modules/.bin/pkg -t node10-$i . -o dist/audio-server-for-skating.exe 
    sha256sum dist/audio-server-for-skating.exe > dist/md256.checksum 
   else
    node_modules/.bin/pkg -t node10-$i . -o dist/audio-server-for-skating-$i
    chmod +x dist/audio-server-for-skating-$i  
    sha256sum dist/audio-server-for-skating-$i >> dist/md256.checksum
   fi
   
done

