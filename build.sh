#!/bin/bash

npm install pkg 
npm install encoding iconv-lite node-fetch node-fs-extra utf8 valid-url xmldom node-fetch node-fs-extra replace-in-file extract-zip

declare -a archs=("win" "mac" "linux")

for i in "${archs[@]}"
do
   echo "Building for $i now."
   if [ "$i" == "win" ]
   then
    pkg -t node10-$i . -o dist/audio-server-for-skating.exe 
    shasum -a 256 dist/audio-server-for-skating.exe > dist/md256.checksum 
   else
    pkg -t node10-$i . -o dist/audio-server-for-skating-$i
    chmod +x dist/audio-server-for-skating-$i  
    shasum -a 256 dist/audio-server-for-skating-$i >> dist/md256.checksum
   fi
   
done

