/* Copyright [2019] [Csaba Anderlik]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

/*! Blue Monday Skin for jPlayer 2.9.2 ~ (c) 2009-2014 Happyworm Ltd ~ MIT License */

/*
 * Skin for jPlayer Plugin (jQuery JavaScript Library)
 * http://www.jplayer.org
 *
 * Skin Name: Blue Monday
 *
 * Copyright (c) 2010 - 2014 Happyworm Ltd
 * Licensed under the MIT license.
 *  - http://www.opensource.org/licenses/mit-license.php
 *
 * Author: Silvia Benvenuti
 * Skin Version: 5.1 (jPlayer 2.8.0)
 * Date: 13th November 2014
 */

var config = require('./config');
const http = require('http');
const {
    parse
} = require('querystring');
const fetch = require('node-fetch');
var DOMParser = require('xmldom').DOMParser;
const fs = require('fs');
const fse = require('node-fs-extra');
const path = require('path');

const replace = require('replace-in-file');

const htmls = "htmls"; //should contain the name of the folder to store the htmls, no trainling / is needed  

var encoding = require('encoding');

var validUrl = require('valid-url');

var tabcss = 'http://localhost:' + config.port + '/assets/text/css/tabulator.min.css';
var jplaycss = 'http://localhost:' + config.port + '/assets/text/css/jplayer.blue.monday.css';
var customcss = 'http://localhost:' + config.port + '/assets/text/css/custom.css';
var tabjs = 'http://localhost:' + config.port + '/assets/text/js/tabulator.min.js';
var jqjs = 'http://localhost:' + config.port + '/assets/text/js/jquery-3.2.1.min.js';
var jplay = 'http://localhost:' + config.port + '/assets/text/js/jquery.jplayer.min.js';
var jplayswf = 'http://localhost:' + config.port + '/assets/text/swf/';

var iconv = require('iconv-lite');
var Buffer = require('buffer').Buffer;

var unorm = require('unorm');

const debug = process.env.DEBUG || 0;

printDebug("Audio Server version " + config.version + " listening on port: " + config.port + " and reading music from ./" + config.music_root, 'info');
let mylocator = {};

// Lets extract the music archive into ./music 
var extract = require('extract-zip')

var zipfile = path.resolve("./" + config.music_root + ".zip");

var outputPath = path.resolve("./" + config.music_root);

extract(zipfile, {
    dir: outputPath
}, function (err) {
    if (err) {
        printDebug("Failed extracting music archive " + err.message, 'error');
    } else {
        printDebug("Successfully extracted the music to: " + outputPath, 'info');
    }
})


var competitionName;

let parseLog = {
    errorLevel: 0
};

let parser = new DOMParser({
    locator: mylocator,
    errorHandler: {
        warning: (msg) => {
            manageXmlParseError(msg, 1, parseLog)
        },
        error: (msg) => {
            manageXmlParseError(msg, 2, parseLog)
        },
        fatalError: (msg) => {
            manageXmlParseError(msg, 3, parseLog)
        },
    },
});

function Category(name, short, free) {
    // always initialize all instance properties
    this.name = name;
    this.short = short;
    this.free = free;

}

function manageXmlParseError(msg, errorLevel, errorLog) {
    if ((errorLog.errorLevel == null) || (errorLog.errorLevel < errorLevel)) {
        errorLog.errorLevel = errorLevel;
    }

    if (errorLog[errorLevel.toString()] == null) {
        errorLog[errorLevel.toString()] = [];
    }

    errorLog[errorLevel.toString()].push(msg);
}

//Create main server, listening on the port specified in config.port, defaults to 8080
const server = http.createServer((req, res) => {

    //Unpack html, css and js dependencies
    if (!fs.existsSync("./assets")) {

        fse.copy(path.join(__dirname, "./assets"), './assets', function (err) {
            if (err) {
                printDebug(err, 'error');
            } else {
                printDebug("Successfully unpacked assets!", 'info');
            }
        });

    }
    //Check if htmls folder exist, create it if not
    if (!fs.existsSync("./htmls")) {
        fs.mkdirSync("./htmls");
    }
    /*
     * replace localhost with hostname from environment variable {SERVER_NAME}
     */
    const options = {
        files: './assets/text/css/jplayer.blue.monday.css',
        from: /localhost/g,
        to: config.host
    };

    try {
        const changedFiles = replace.sync(options);
    } catch (error) {
        printDebug('Error occurred:', 'error');
    }

    //serve requests
    if (req.method === 'POST') {
        collectRequestData(req, result => {
            var day = result.day;
            var competitionURL = result.url;
            var replyHTML;

            if (!validUrl.isUri(competitionURL)) {
                res.writeHead(500);
                res.end('Invalid url: ' + competitionURL + ' was submitted!   ..\n Please go back and try again.');
                res.end();
                return;
            }

            if (competitionURL.indexOf('index') != -1) {
                competitionURL = competitionURL.slice(0, competitionURL.indexOf('index'));
            }

            while (competitionURL.charAt(competitionURL.length - 1) == "/") {
                competitionURL = competitionURL.slice(0, -1)
            }

            competitionName = competitionURL.slice(competitionURL.lastIndexOf('/') + 1);

            (async () => {

                await fetch(competitionURL).then(res => res.buffer())
                    .then(function (buffer) {


                        //parse URL of the main event page to fetch categories/competitors/events 

                        var htmlDoc = parser.parseFromString(iconv.decode(buffer, "ISO-8859-1").toString(), 'text/html');

                        var tables = htmlDoc.getElementsByTagName("table");

                        var rows;

                        //dirty we need to check if the table has any rows, if no then jump to the next table; 

                        for (var n = 0; n < htmlDoc.getElementsByTagName("table").length; n++) {
                            rows = tables[n].getElementsByTagName("tr");
                            if (rows != null && rows.length > 0)
                                break;
                        }


                        let timeScedule = tables[n + 1];
                        tsRows = timeScedule.getElementsByTagName("tr");

                        var competitionDays = [];
                        dayIndex = 0;
                        var tsCategories = [];
                        var categories = {}; // will store the categories and events
                        
                        for (var j = 1; j < tsRows.length; j++) {

                            var tsE = tsRows[j].getElementsByTagName("td");
                            
                            if (tsE.length == 1) {
                                dayIndex = dayIndex + 1;
                                tsCategories = [];
                            }

                            if (tsE.length > 1) {
                                tsCategories.push(tsE.item(2).firstChild.nodeValue.replace("/", "")+":"+tsE.item(3).getElementsByTagName("a").item(0).firstChild.nodeValue);
                                competitionDays[dayIndex - 1] = tsCategories;
                            }

                        }

                        printDebug("Time Schedule: " + JSON.stringify(competitionDays), 'debug');


                        if (rows == undefined) {

                            res.writeHead(500);
                            res.end('The provided url: ' + result.url + ' does not contain event data!   ..\n Please go back and try again.');
                            res.end();
                            return;

                        }

                        var dict = [];

                        for (var i = 1; i < rows.length; i++) {

                            var cols = rows[i].getElementsByTagName("td");


                            if (cols.item(0).childElementCount != 0)
                                if (cols.item(0).firstChild != null && cols.item(0).firstChild.nodeValue != null) {

                                    dict.push({
                                        key: i,
                                        value: cols.item(0).firstChild.nodeValue
                                    });

                                }

                        }

                        
                        for (var item in dict) {
                            var short = "";
                            var free = "";
                            //add the option to create the htmls, before the Result pages are populated and use the Entries pages instead.
                            //set the environment variable: Debug to 5.
                            if (debug == 5) {
                                if (rows[dict[item].key].getElementsByTagName("td").item(2).getElementsByTagName("a").item(0).firstChild.nodeValue.indexOf("Entries") !== -1) {
                                    free = competitionURL + "/" + rows[dict[item].key].getElementsByTagName("a")[0].getAttribute("href");
                                    printDebug("Setting free programe URL to : " + free + " for: " + dict[item].value, 'debug');
                                }
                            } else {
                                if (rows[dict[item].key + 1].getElementsByTagName("td").item(1).firstChild.nodeValue == "Kortprogram" || rows[dict[item].key + 1].getElementsByTagName("td").item(1).firstChild.nodeValue == "Short Program") {
                                    short = competitionURL + "/" + rows[dict[item].key + 1].getElementsByTagName("a")[1].getAttribute("href");
                                    free = competitionURL + "/" + rows[dict[item].key + 2].getElementsByTagName("a")[1].getAttribute("href");
                                } else {
                                    free = competitionURL + "/" + rows[dict[item].key + 1].getElementsByTagName("a")[1].getAttribute("href");
                                }
                            }
                             var categoryName = dict[item].value.replace("/", "");
                            
                             categories[categoryName] = new Category(categoryName, short, free);

                        }
                        printDebug("Categories: " + JSON.stringify(categories), 'debug');
                        //prepare reply with the HTML containing the categories with links to the entry lists. 

                        replyHTML = "<!doctype html><html><head><meta http-equiv=\"Content-Type\" content=\"text/html;charset=UTF8\"></head><body> Program dag: " + day + " <table><tr bgcolor='CC33CC'> <td>Category</td><td>Segment</td></tr>";

                        var no_music = [];

                        if (day > competitionDays.length) {

                            res.writeHead(500);
                            res.end('This competition has only ' + competitionDays.length + " days. Please choose a day between 1 and " + competitionDays.length + ' and try again.');
                            res.end();
                            return;

                        }


                        for (l=0; l < competitionDays[day - 1].length; l++){
                         
                            
                            
                            categoryName = competitionDays[day - 1][l].split(":")[0];
                            var segmentName  = competitionDays[day - 1][l].split(":")[1];

                            var category = categories[categoryName];

                            

                            if (segmentName == "Kortprogram" || segmentName == "Short Program") {
                                printDebug("Segment short: " +segmentName+";", 'debug');
                                url = category.short;
                                tableRow = "<tr bgcolor='#FF8888'> <td>" + category.name + "</td><td><a href=\"./" + htmls + "/" + competitionName + "/" + category.name.replace(/ /g, "_") + "_" + "day_" + day + ".html\">Kortprogram</a></td></tr>";

                            } else {
                                printDebug("Segment free: " +segmentName+";", 'debug');
                                url = category.free;
                                tableRow = "<tr bgcolor='#FF8888'> <td>" + category.name + "</td><td><a href=\"./" + htmls + "/" + competitionName + "/" + category.name.replace(/ /g, "_") + "_" + "day_" + day + ".html\">Friløp</a></td></tr>";
                                
                            }

                            replyHTML = replyHTML + tableRow;

                                createList(category, segmentName, day, writeHtml, function (listWrongCompetitors) {
                                    for (var t = 0; t < listWrongCompetitors.length; t++) {
                                        no_music.push(listWrongCompetitors[t]);
                                    }

                                    //Let us write the competitors with missing music files into alert.html
                                    if (no_music.length != 0) {
                                        var alertCompetitors = "<!doctype html><html><head><meta http-equiv=\"Content-Type\" content=\"text/html;charset=UTF8\"></head><body> Please check the music files for the following competitors. Correct the missing music files or incorect names and return to the main page.<table>";
                                        for (var m = 0; m < no_music.length; m++) {
                                            alertCompetitors = alertCompetitors + "<tr bgcolor='#FF8888'><td>" + no_music[m].name + "</td><td>" + no_music[m].club + "</td><td>" + no_music[m].category.split(' ')[0] + "</td><td>" + no_music[m].segment + "</td></tr>";
                                        }
                                        alertCompetitors = alertCompetitors + "</table></body></html>";
                                        writeHtml("htmls/" + competitionName + "/alert_" + day + ".html:" + alertCompetitors, 0);
                                    } else
                                        writeHtml("htmls/" + competitionName + "/alert_" + day + ".html:" + "<!doctype html><html><head><meta http-equiv=\"Content-Type\" content=\"text/html;charset=UTF8\"></head><body>No errors found!</body></html>", 0);
                                });



                        }

                        replyHTML = replyHTML + "<tr bgcolor='#FF8888'> <td></td><td><a href=\"./htmls/" + competitionName + "/alert_" + day + ".html\">Check here for any errors!</a></td></tr></table></body></html>";

                        writeHtml(competitionName + '_index_day_' + day + ".html:" + replyHTML, 0);

                        res.end(replyHTML);

                    }).catch((error) => {
                        printDebug(error, 'error');
                        res.writeHead(500);
                        res.end('We have encountered the following error: ' + e.message + ' ..\n');
                        res.end();
                        return;
                    });
            })()
        });
    } else if (req.url.indexOf(".html") > 0 || req.url.indexOf(".mp3") > 0 || req.url.indexOf(".js") > 0 || req.url.indexOf(".css") > 0 || req.url.indexOf(".jpg") > 0) {
        printDebug('Handling request for ' + req.url, 'debug');

        var filePath = '.' + req.url;

        var extname = path.extname(filePath);
        //serve the htmls and mp3 music files
        var contentType = 'text/html';
        switch (extname) {
            case '.js':
                contentType = 'text/javascript';
                break;
            case '.css':
                contentType = 'text/css';
                break;
            case '.json':
                contentType = 'application/json';
                break;
            case '.png':
                contentType = 'image/png';
                break;
            case '.jpg':
                contentType = 'image/jpg';
                break;
            case '.mp3':
                contentType = 'audio/mp3';
                filePath = req.url;
                break;
        }

        //decode and normalize the filepath from the request
        if (process.platform == "darwin") {
            filePath = unorm.nfd(decodeURI(filePath).slice(filePath.indexOf(":") + 1));
        } else {
            filePath = unorm.nfc(decodeURI(filePath).slice(filePath.indexOf(":") + 1));
        }
        //stream back the requested file
        fs.readFile(filePath, function (error, content) {
            if (error) {
                printDebug("Error when streaming: " + filePath, 'error');
                printDebug(error.message, 'error');
                res.writeHead(500);
                res.end('We have encountered the following error: ' + error.message + ' ..\n');
                res.end();
            } else if (contentType == "audio/mp3" || contentType == "image/jpg") {
                var stream = fs.createReadStream(filePath);
                var stat = fs.statSync(filePath);
                printDebug("Streaming file " + filePath + "...", 'debug');
                stream.on('error', function (error) {
                    res.writeHead(404, 'Not Found');
                    res.end();
                });
                res.writeHead(200, {
                    'Content-Type': contentType,
                    'Content-Length': stat.size,
                    'Accept-Ranges': "bytes"
                });
                stream.pipe(res);
            } else {
                res.writeHead(200, {
                    'Content-Type': contentType
                });
                res.end(content, 'utf-8');
            }
        });
    } else { //send the form to get the URL and the day 1 or 2 
        (async () => {
            const inputForm = await fs.readFileSync(path.join(__dirname, './assets/text/html/input_form.html')).toString();
            res.end(inputForm);
        })()
    }
});
server.listen(config.port);

function collectRequestData(request, callback) {
    const FORM_URLENCODED = 'application/x-www-form-urlencoded';
    if (request.headers['content-type'] === FORM_URLENCODED) {
        let body = '';
        request.on('data', chunk => {
            body += chunk.toString();
        });
        request.on('end', () => {
            callback(parse(body));
        });
    } else {
        callback(null);
    }
}

/*
 * Prepare the playlist page based on the order retrieved from the starting order page. 
 */
async function createList(category, segmentName, day, callback1, callback2) {
    //main fuction to create the array of competitors with starting number, name, club and path to music file
    var competitors = [];
    var no_music_competitors = [];
    var url = "";
    var musicRoot = "";

    var musicCategory;
    var segment;

    musicCategory = category.name.split(' ')[0];

    /*
    if (day == 2) {
        url = category.free;
        segment = "Friløp";

    } else if (day == 1) {

        if (category.short != "") {
            url = category.short;
            segment = "Kortprogram";

        } else {

            url = category.free;
            segment = "Friløp";

        }
    }
    */
    
   if (segmentName == "Kortprogram" || segmentName == "Short Program") {
       url = category.short;
       segment = "Kortprogram";
   }
   else {
       url = category.free;
       segment = "Friløp";
   }



    musicRoot = "music/";

    printDebug("Fetching URL: " + url, 'debug');

    try {
        if (url != "")
            fetch(url)
            .then(res => res.buffer()).then(function (buffer) {
                var competitors = [];

                var htmlc = parser.parseFromString(iconv.decode(buffer, "ISO-8859-1").toString(), 'text/html');

                var rows;
                var tables = htmlc.getElementsByTagName("table");

                //nasty logic to parse entries
                for (var n = 0; n < htmlc.getElementsByTagName("table").length; n++) {
                    rows = tables[n].getElementsByTagName("tr");
                    if (rows != null && rows.length > 0) {
                        break;
                    }

                }

                k = 0;
                for (var i = 1; i < rows.length; i++) {
                    var entry = [];
                    var cols = rows[i].getElementsByTagName("td");
                    if (cols != null) {
                        for (j = 0; j < 3; j++) {

                            if (cols.item(j) != null) {
                                if ((cols.item(j).childElementCount != 0) && (cols.item(j).firstChild != null) && (cols.item(j).firstChild.nodeValue != null)) {
                                    entry[j] = cols.item(j).firstChild.nodeValue;
                                } else if (cols.item(j).getElementsByTagName("a").length != 0) {

                                    entry[j] = cols.item(j).getElementsByTagName("a")[0].firstChild.nodeValue;
                                }
                            }
                        }
                        //set the club entry to space for the "Warm-up group" 
                        if (entry[0] == null) {
                            entry[2] = " ";
                        }

                        var competitor = new Competitor(entry[0], entry[1], entry[2], category.name, segment);
                        printDebug("Competitor: " + JSON.stringify(competitor), 'trace');


                        if ((competitor.number != null) && (competitor.name != null)) {

                            competitor.music = findMusicFileName(competitor.name, musicRoot, musicCategory, segment);
                            if ((competitor.music == null) && (competitor.club != null)) {
                                printDebug('Could not find the music for competitor: ' + competitor.name, 'warning');
                                no_music_competitors.push(competitor);
                            }
                        } else if (competitor.number == null) {
                            competitor.music = path.resolve(musicRoot) + "/warmup/" + competitor.name.trim() + ".mp3";
                        }
                        if (competitor.club != null) {
                            competitor.index = k;
                            k = k + 1;
                            competitors.push(competitor);
                        }

                    }
                }

                //nasty logic ends here

                callback1(category.name + "_day_" + day + ".html:" + iconv.decode(Buffer.from(JSON.stringify(competitors)), "UTF-8").toString());
                callback2(no_music_competitors);
            });


    } catch (e) {
        printDebug(e, 'error');
    }
}


/*
 * Function to write the html to disk, convention text=filename:html_content; mode=0 writes the html only, while mode=1 adds the contents of top.html and bottom.html
 */
function writeHtml(text, mode = 1) {

    var html = text.slice(text.indexOf(":") + 1);

    var htmlFile = text.split(":")[0];

    var dir = "./htmls/" + competitionName;

    if (mode == 1) {
        htmlFile = htmlFile.replace(/ /g, "_").replace(/\//g, "");
    }

    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
        printDebug('Creating directory: ' + dir, 'info');
    }

    (async () => {

        const top = await fs.readFileSync(path.join(__dirname, './assets/text/html/top.html')).toString().replace(/TABCSS/, tabcss).replace(/TABJS/, tabjs).replace(/JQJS/, jqjs).replace(/JPLAY/, jplay).replace(/JPCSS/, jplaycss).replace(/CUSTCSS/, customcss).replace(/localhost/g, config.host);

        const bottom = await fs.readFileSync(path.join(__dirname, './assets/text/html/bottom.html')).toString().replace(/8083/g, config.port).replace(/localhost/g, config.host);



        if (mode == 1) {
            html = top + html + bottom;
            htmlFile = "./" + htmls + "/" + competitionName + "/" + htmlFile;
        }

        fs.writeFileSync(htmlFile, html, function (err) {
            if (err) {
                printDebug('Saving file ' + htmlFile + ' failed. ' + err.msg, 'error');
            }
            printDebug('File ' + htmlFile + ' successfully saved!', 'info');
        });

    })();
}


function Competitor(number, name, club, category, segment) {
    // always initialize all instance properties
    this.number = number;
    this.index = 0;
    this.name = name;
    this.club = club;
    this.music = ""; // default value
    this.segment = segment; //Kortprogram or Friløp
    this.category = category;
}

/*
 * Function to search the file name matching the competitors name return the result in musicFile, musicFolder is based on the category information  
 * The logic is that we take split the name by spaces and run through the file names, and find the one which contains all the parts of the name.
 */
function findMusicFileName(name, musicFolder, musicCategory, segment, musicFile) {
    var hits = 0;

    var res;
    if (process.platform == "darwin") {
        res = name.toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, "").split(" ");
    } else
        res = name.toLowerCase().normalize('NFC').replace(/[\u0300-\u036f]/g, "").split(" ");
    try {
        fs.readdirSync(musicFolder)
            .forEach(directory => {
                if (directory.indexOf(musicCategory) !== -1) {
                    //handle nested directory structure like Junior/Junior A/Kortprogram 
                    fs.readdirSync(musicFolder + "/" + directory).forEach(underDirectory => {
                        if (underDirectory.indexOf(musicCategory) !== -1 || underDirectory.indexOf(segment) !== -1) {

                            if (underDirectory.indexOf(segment) !== -1) {

                                underDirectory = ".";

                            }
                            fs.readdirSync(musicFolder + "/" + directory + "/" + underDirectory + "/" + segment).forEach(file => {
                                var fname = "";
                                if (process.platform == "darwin") {
                                    fname = path.parse(file).name.toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, "");
                                } else {
                                    fname = path.parse(file).name.toLowerCase().normalize('NFC').replace(/[\u0300-\u036f]/g, "");
                                }
                                const filepath = path.resolve(musicFolder + "/" + directory + "/" + underDirectory + "/" + segment, file);
                                printDebug("Looking for: " + filepath, 'trace');
                                for (var k = 0; k < res.length; k++) {
                                    if (fname.indexOf(res[k]) !== -1) {
                                        hits = hits + 1;
                                    }
                                }
                                if (hits === res.length) {
                                    musicFile = filepath;
                                }
                                hits = 0;
                            });
                        }

                    });
                }
            });
    } catch (error) {
        printDebug(error.message, 'error');
    }
    return musicFile;
}
/** Utility to print debug*/
function printDebug(message, level) {
    levelValue = 0;

    switch (level) {
        case 'info':
            levelValue = 0;
            break;
        case 'error':
            levelValue = 0;
            break;
        case 'warning':
            levelValue = 0;
            break;
        case 'debug':
            levelValue = 1;
            break;
        case 'trace':
            levelValue = 2;
            break;
    }

    if (levelValue <= debug) {
        console.log("[" + level + "]: " + message);
    }
}