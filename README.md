## README
Small node.js based server for audio in figure skating events. Can only be used in connection to ISUCalc generetad HTML files containing the list of entries. It also needs the music tracks for the programes

## Build

The executable can be built using https://www.npmjs.com/package/pkg and node.js. 

1. clone this repository
2. install node.js from https://nodejs.org/en/
3. run: npm install -g pkg 
4. npm install encoding iconv-lite node-fetch node-fs-extra utf8 valid-url xmldom node-fetch node-fs-extra replace-in-file
5. run: pkg -t <target_machine> . (<target_machine> can be node<version>-win, for instance node10-win for Windows or node10-linux for Linux, see pkg --help for a full list of options). Alternatively, one can run the included build.sh script. This will create the executables in the dist folder.  

## RUN 

Either build or use one of the pre-build binaries in the download folder: https://bitbucket.org/cs-ande/audio-server-for-skating/downloads/ folder. 

The resulting binary has to be copied in the folder where the music archive has been downloaded (from sportsadmin/arrangør page, save as music.zip), default folder name music and naming convention: 
  music/<category_name>/Friløp or Kortprogram/<competitor_name>_<club name>_ ....mp3 (the name of the competitor has to be included in the file name. 

Double click the executable and navigate to http://localhost:8080 with your preferred web-browser. 
Enter the address of the competition event and either 1 or 2 on the lower text field (day 1 of the event or day 2 of the event.)

And click submit, the next page will contain the table with the categories for the choosen day of the event.  
## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).
